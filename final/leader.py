from init import *
class Leader():
    """Leader is the class that sends shape requests and and picks out the best 
       estimation of the shape. Any of the cameras can be the leader (specified
       in parse arguments), but there should only be one in the system"""
    def __init__(self,ncam, col):
        # Init the values
        self.n_cam = ncam # maximum number of cameras
        self.color = col # color to track
        self.init_connection()
        

    def init_connection(self):
        # Create a ZMQ_PUB socket and bind publisher to all interfaces
        context = zmq.Context()     

        # Create and bind the publisher side (port 8000 not to interfere with the leaders camera publisher)
        self.publisher = context.socket(zmq.PUB)
        self.publisher.bind("tcp://*:%i" % 8000)

        # Create subsciber and connect to all the cameras (including its own camera)
        self.subsciber = context.socket(zmq.SUB)

        for x in range(1,6):
            self.subsciber.connect("tcp://192.168.1.15"+str(x)+":9000")

        self.subsciber.subscribe("Shape reply")

    def send_request(self):
        # Send a request for the string
        req = ShapeRequest()
        req.color = self.color # random info to fill the packet
        binaryReq = req.SerializeToString()
        self.publisher.send_multipart([str.encode("Shape request"), binaryReq])


    def receive_message(self):
        # Receive a single reply from a camera 
        # or continue to receive another if the message does not come in a time limit

        # Start measuring time
        start = time.time()

        while True:
            try:
                # Use non-blocking variant of reveiving the message
                string = self.subsciber.recv_multipart(flags = zmq.NOBLOCK)
                break
            # if no message is received continue this exception
            except zmq.ZMQError:
                # Set a timeout for the reply if it is not received in time
                if time.time() - start > 0.3:
                    # Exit the function with a no reply with very low confidence
                    return ("No signal", 10)

        # Continue if message was received
        pred = ShapeReply()

        # Try to parse the message (sometimes results in errors) 
        try:
            pred.ParseFromString(string[1])
        except DecodeError:
            return ("No signal", 10)

        print(str(pred.cameraID) + " " + str(pred.shape)+ " - score: "  + str(round(1/pred.confidence*100)))

        return (pred.shape,pred.confidence)


    def vote(self,preds):
        # Choosing the best of the proposals
        sorted_preds = sorted(preds.items(), key=lambda kv: kv[1])
        best_pred = sorted_preds[-1][0]
        confidence = sorted_preds[-1][1]
        
        return best_pred, confidence

    def communication(self):
        # Main loop of the class

        while True:

            # Create dictionary
            predictions = {}
            conf_sum = 0

            # Send a shape request
            self.send_request()
            #Receive (or timeout) n_cam anwsers
            for i in range(0,self.n_cam):
                pred,conf = self.receive_message()

                # Sum all predictions by name and invert the score (before that, the lower the score better the match)
                if pred in predictions:
                    predictions[pred]+=1/conf
                else:
                    predictions[pred]=1/conf
                conf_sum += 1/conf

            # Get the best prediciton and its confidence
            best_pred, conf = self.vote(predictions)

            # Create a percentage confidence based on all the propostions
            confidence = round(conf/conf_sum * 100)
            print ("Best pred: " + str(best_pred)+ " - " + str(confidence) + " %\n" )

            # Use the leader's camera object to send the best predition to the GUI
            # (it already has a connection with the GUI and reserves the ip address and port)
            self.send_to_gui(best_pred,confidence)

            # Delay between sending another request
            time.sleep(2)


    def send_to_gui(self, best_pred, conf):
        # Send best prediction to the Gui predition slot
        cmd = AdditionalControl()
        cmd.command = str(best_pred) + " - " + str(conf) + " %"
        cmd.cameraID = 0
        binaryCmd = cmd.SerializeToString()
        self.publisher.send_multipart([str.encode("Prediction"), binaryCmd])