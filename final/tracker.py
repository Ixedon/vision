from init import *
from condet import ShapeDetector


class Tracker():
    """Tracker is a class that given an input image and predifined color to track 
    results in the predicted shape (using ShapeDetector), 
    it's confidence and a message with a predifined bounding box """
    def __init__(self, track = True):
        # Initialize class variables and toggle tracking
        self.last_cnts = None
        self.image = None
        self.track = track

        # Initial color before getting the first request from the leader
        self.color = "green"
        self.sc = None

    def threshold(self):
        # Get shape based on image

        # Select the hsv color bounds for tracking (use range_detector.py to get theese values)
        
        #green ball
        greenLower = (46, 43, 0)
        greenUpper = (88, 255,255)

        #yellow star
        yellowLower = (20,59,113)
        yellowUpper = (33,255,255)

        #redish cube
        redLower = (130,85,0)
        redUpper = (255,255,120)


        if self.color in ("green","g"):
            lower = greenLower
            upper = greenUpper
        elif self.color in ("yellow", "y"):
            lower = yellowLower
            upper = yellowUpper
        else:
            lower = redLower
            upper = redUpper

        # Apply small blur and convert image to hsv
        blurred = cv2.GaussianBlur(self.image, (11, 11), 0)
        hsv = cv2.cvtColor(blurred, cv2.COLOR_BGR2HSV)

        # Extract only parts of the image that match the color range

        mask = cv2.inRange(hsv,lower, upper)
        mask = cv2.erode(mask, None, iterations=2)
        mask = cv2.dilate(mask, None, iterations=2)

        #cimg = self.image.copy()

        # Find contours of images left
        cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        cnts = imutils.grab_contours(cnts)

        # Initialize the shape detector if it is not already initialized (compares with predifined models)
        if self.sc == None:
            self.sc = ShapeDetector()

        shape = "No shape"
        obj_cnts = None
        conf = 1

        # Check if any contours are found
        if len(cnts) > 0:

            # Select only the the 5 biggest ones and keep them sorted by size
            cnts = sorted(cnts, key = cv2.contourArea) [-5:]

            # Initialize countours from last frame
            if self.last_cnts is None:
                self.last_cnts = cnts

            # Filter out too small contours and ones that changed too much since the last frame
            comp_cnts = []
            for c,c_old in zip(cnts,self.last_cnts):
                if cv2.matchShapes(c,c_old,1,10.0) < 0.05 and cv2.contourArea(c) > 200:
                    comp_cnts.append((c, c_old))

            # Sort contours by score that include:
            comp_cnts = sorted(comp_cnts,key = lambda x: 
                # 1. Distance that the center of mass of the contour has moved since the last frame
                (abs(int(cv2.moments(x[0])["m10"] / cv2.moments(x[0])["m00"])
                -int(cv2.moments(x[1])["m10"] / cv2.moments(x[1])["m00"]))
                +abs(int(cv2.moments(x[0])["m01"] / cv2.moments(x[0])["m00"])
                -int(cv2.moments(x[1])["m01"] / cv2.moments(x[1])["m00"])))
                # 2. The change in shape since the last frame
                * 1/(cv2.matchShapes(x[0],x[1],1,10.0)+0.00001)
                )

           # Take the contour with the best score and get a shape prediction with confidence
            if len(comp_cnts) > 0:
                # Take just the new contours
                tracked_cnts, _ = zip(*comp_cnts)

                # Select the one with the highest score (last one in sorted list)
                obj_cnts = tracked_cnts[-1]
                # Retreive the shape and confidce from the shape detector
                shape,conf = self.sc.detect(obj_cnts)

            # Update the last contour
            self.last_cnts = cnts

        return (shape,obj_cnts,conf)

    
    def tracker(self,image):
        # Return values based on the shape result
        self.image = image

        shape,obj_cnts,conf = self.threshold()
        # Unless there is no contours, create a bounding box and return it along with the image
        try:
            box = cv2.boundingRect(obj_cnts)
        except TypeError:
          pass
        return (image,box,shape,conf)

    def no_tracker(self,image):
        # Return image with empty bounding box
        return (image,(0,0,0,0),"none","No tracking")



    def run(self,image = None, color = ""):
        # Main function ran inn order to use
        self.color = color
        if self.track:
            ret = self.tracker(image)
        else:
            ret = self.no_tracker()
        return ret


    