import zmq
import numpy as np
import sys
import cv2
import time, math
import base64
from random import randint
from Message_pb2 import *
#import protobuf3 as pb
from PIL import Image
import imutils

k = 0


def threshold(last_cnts):
    ballLower = (16, 0, 176)
    ballUpper = (164, 255, 255)

    greenLower = (25, 53, 80)
    greenUpper = (91, 255, 255)

    blurred = cv2.GaussianBlur(image, (11, 11), 0)
    hsv = cv2.cvtColor(blurred, cv2.COLOR_BGR2HSV)

    mask = cv2.inRange(hsv, ballLower, ballUpper)
    #mask = cv2.inRange(hsv, greenLower, greenUpper)
    mask = cv2.erode(mask, None, iterations=2)
    mask = cv2.dilate(mask, None, iterations=2)

    res = cv2.bitwise_and(image,image,mask = mask)
    cv2.imshow("res", res)

    cimg = image.copy()

    cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    cnts = imutils.grab_contours(cnts)
    #cv2.drawContours(cimg, cnts, -1, (0,255,0), 3)

    if len(cnts) > 0:

        #mean_arr = []
        

        # for c in cnts:
            
        #     # x, y, w, h = cv2.boundingRect(c)
        #     # cv2.rectangle( image, (x, y), (x + w, y + h), (255, 0, 255), 2, 8, 0 )  #bounding box
        #     mask2 = np.zeros(mask.shape,np.uint8)
        #     cv2.drawContours(mask2,[c],0,255,-1)
        #     mean_val = cv2.mean(image,mask = mask2)
        #     mean_arr.append(mean_val)
        #cnts = [c for c in cnts if cv2.contourArea(c) > 1000]
        cnts = sorted(cnts, key = cv2.contourArea) [-5:]

        if last_cnts is None:
            last_cnts = cnts
            print ("filles")

        # for c in cnts:
        #     for c_old in last_cnts:
       # cv2.drawContours(cimg, cnts, -1, (0,255,0), 3) 

        #zipped = zip(mean_arr,cnts)
        #mean_arr, cnts = zip(*sorted(zipped, key=lambda x: x[0]))
        #cnts = sorted(cnts, key=lambda x: (cv2.boundingRect(x)))

        # if k > 100:
        #     cp = image.copy()
        #     cp1 = image.copy()
        #     for c,c0 in zip(cnts,last_cnts):
        #         cp = image.copy()
        #         cp1 = image.copy()
        #         cv2.drawContours(cp, [c], -1, (255,0,0), 3)
        #         cv2.drawContours(cp1, [c0], -1, (255,0,0), 3)
        #         cv2.imshow("comp", np.hstack([cp, cp1]))
        #         cv2.waitKey(0)
        comp_cnts = []
        for c,c_old in zip(cnts,last_cnts):
            #print (cv2.matchShapes(c,c_old,1,0.0))
            if cv2.matchShapes(c,c_old,1,10.0) < 0.05 and cv2.contourArea(c) > 200:# and \
             #abs(cv2.contourArea(c) - cv2.contourArea(c_old)) < 20:
            #if cv2.contourArea(c) > 50:
                comp_cnts.append((c, c_old))
            # else:
            #     print (cv2.contourArea(c))
               #cv2.drawContours(cimg, [c], -1, (255,0,0), 3)
               # x, y, w, h = cv2.boundingRect(c)
               # cv2.rectangle( cimg, (x, y), (x + w, y + h), (255, 0, 255), 2, 8, 0 )  #bounding box
            #print (abs(cv2.contourArea(c) -1)) 
            #if abs(cv2.contourArea(c) - cv2.contourArea(c_old)) < 10:
        #print (len(comp_cnts))
        
        comp_cnts = sorted(comp_cnts, #zip(cnts,last_cnts), 
            key = lambda x: #abs(cv2.boundingRect(x[0])[0] - cv2.boundingRect(x[1])[0])
            #+ abs(cv2.boundingRect(x[0])[1] - cv2.boundingRect(x[1])[1])
            (abs(int(cv2.moments(x[0])["m10"] / cv2.moments(x[0])["m00"])
            -int(cv2.moments(x[1])["m10"] / cv2.moments(x[1])["m00"]))
            +abs(int(cv2.moments(x[0])["m01"] / cv2.moments(x[0])["m00"])
            -int(cv2.moments(x[1])["m01"] / cv2.moments(x[1])["m00"])))
            * 1/(cv2.matchShapes(x[0],x[1],1,10.0)+0.00001)
            #*1/abs(cv2.contourArea(x[0]) - cv2.contourArea(x[1])+ 0.00001) 
            )
             #* cv2.contourArea(x[0]) 
             #*1/math.sqrt(cv2.contourArea(x[0])) - cv2.contourArea(x[1]))

        # #print(math.fsum(cv2.boundingRect(cnts[0])) - math.fsum(cv2.boundingRect(last_cnts[0])))
        if len(comp_cnts) > 0:
            tracked_cnts, _ = zip(*comp_cnts)
            #cv2.drawContours(cimg, tracked_cnts, -1, (0,255,255), 3)  

            obj_cnts = tracked_cnts[-1]
            cv2.drawContours(cimg, [obj_cnts], -1, (0,0,255), 3)

            x, y, w, h = cv2.boundingRect(obj_cnts)
            cv2.rectangle( cimg, (x, y), (x + w, y + h), (255, 0, 0), 2, 8, 0 )  #bounding box
            # obj_cnts = tracked_cnts[-2]
            # cv2.drawContours(cimg, [obj_cnts], -1, (100,0,0), 3)

            # obj_cnts = tracked_cnts[-3]
            # cv2.drawContours(cimg, [obj_cnts], -1, (0,0,100), 3)

            # obj_cnts = tracked_cnts[-4]
            # cv2.drawContours(cimg, [obj_cnts], -1, (0,100,0), 3)


            if k > 100:
                cv2.waitKey(0)

        last_cnts = cnts
        #cv2.imshow("mask", mask2)


    cv2.imshow("count", cimg)
    return last_cnts
    


if __name__=="__main__":
    cap = cv2.VideoCapture('balls-30.avi')       #device 0 is video capture device  
    #cap = cv2.VideoCapture('greenball.avi') 
    last_cnts = None

    while True:
        ret, image = cap.read()
        #cv stuff here


        
        last_cnts = threshold(last_cnts)



        #cv2.imshow("Stream", frame)
        key = cv2.waitKey(30) & 0xff
        if key == 27 or key == ord('q'):
           break
        #print ("recieved " + str(k))
        k = k + 1
    cap.release()
    cv2.destroyAllWindows()