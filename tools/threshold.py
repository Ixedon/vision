import numpy as np
import argparse
import cv2
import imutils

ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required = True, help = "Path to the image")
args = vars(ap.parse_args())


image = cv2.imread(args["image"])

ballLower = (16, 0, 176)
ballUpper = (164, 255, 255)

whiteLower = (16, 0, 176)
whiteUpper = (164, 50, 255)

yellowLower = (25, 70, 177)
yellowUpper = (255, 255, 255)

greenLower = (25, 53, 80)
greenUpper = (91, 255, 255)


#image = imutils.resize(image, width=600)
blurred = cv2.GaussianBlur(image, (11, 11), 0)
hsv = cv2.cvtColor(blurred, cv2.COLOR_BGR2HSV)

mask = cv2.inRange(hsv, ballLower, ballUpper)
#mask = cv2.inRange(hsv, greenLower, greenUpper)
mask = cv2.erode(mask, None, iterations=2)
mask = cv2.dilate(mask, None, iterations=2)



mask_white = cv2.inRange(hsv, whiteLower, whiteUpper)
mask_white = cv2.erode(mask_white, None, iterations=2)
mask_white = cv2.dilate(mask_white, None, iterations=2)


mask_yellow = cv2.inRange(hsv, yellowLower, yellowUpper)
mask_yellow = cv2.erode(mask_yellow, None, iterations=2)
mask_yellow = cv2.dilate(mask_yellow, None, iterations=2)


#mask = mask_yellow
#mask = mask_white

res = cv2.bitwise_and(image,image,mask = mask)
cv2.imshow("res", res)



# cv2.imshow("res-y", mask_yellow)

# cv2.imshow("res-w", mask_white)
# res = cv2.bitwise_and(image,image,mask = cv2.bitwise_or(mask_white,mask_yellow))
# cv2.imshow("res2", res)
# getthe(mask)

#cv2.imshow("mask", mask)
#cv2.waitKey(0)


gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

gray = cv2.GaussianBlur(gray,(5,5),0);
gray = cv2.medianBlur(gray,5)
gray = cv2.adaptiveThreshold(gray,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,\
           cv2.THRESH_BINARY,11,3.5)

cv2.imshow("just the", gray)
#cv2.waitKey(0)


#gray = cv2.cvtColor(mask, cv2.COLOR_BGR2GRAY)

gray = cv2.GaussianBlur(mask,(5,5),0);
gray = cv2.medianBlur(gray,5)
gray = cv2.adaptiveThreshold(gray,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,\
           cv2.THRESH_BINARY,11,3.5)

cv2.imshow("both", gray)





# gray = cv2.cvtColor(res, cv2.COLOR_BGR2GRAY)

# gray = cv2.GaussianBlur(gray,(5,5),0);
# gray = cv2.medianBlur(gray,5)
# gray = cv2.adaptiveThreshold(gray,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,\
#            cv2.THRESH_BINARY,11,3.5)

# cv2.imshow("after mask", gray)


cv2.waitKey(0)



cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
cnts = imutils.grab_contours(cnts)
cv2.drawContours(image, cnts, -1, (0,255,0), 3)
# for c in cnts:
#     cv2.drawContours(image, [c], -1, (0,0,255), 3)
#     cv2.imshow("count", image)
#     cv2.waitKey(0)


cv2.imshow("count", image)
cv2.waitKey(0)


# output = image.copy()

# circles = cv2.HoughCircles(gray, cv2.HOUGH_GRADIENT, 3, 1000, param2 = 50)  #pilka 2-3 ziel: min 6
 
# print("tu")
# # ensure at least some circles were found
# if circles is not None:
#     # convert the (x, y) coordinates and radius of the circles to integers
#     circles = np.round(circles[0, :]).astype("int")
 
#     # loop over the (x, y) coordinates and radius of the circles
#     for (x, y, r) in circles:
#         # draw the circle in the output image, then draw a rectangle
#         # corresponding to the center of the circle
#         cv2.circle(output, (x, y), r, (0, 255, 0), 4)
#         cv2.rectangle(output, (x - 5, y - 5), (x + 5, y + 5), (0, 128, 255), -1)
#         print (x,y)
#     # show the output image
#     cv2.imshow("output", output)
#     cv2.waitKey(0)
# else:
#     print ("no circles")