import zmq
import numpy
import sys
import cv2
import time
from random import randint
from Message_pb2 import *
#import protobuf3 as pb
import argparse
from PIL import Image


ap = argparse.ArgumentParser()
ap.add_argument("-o", "--output", required=True,
    help="path to output video file")
ap.add_argument("-p", "--picamera", type=int, default=-1,
    help="whether or not the Raspberry Pi camera should be used")
ap.add_argument("-f", "--fps", type=int, default=20,
    help="FPS of output video")
ap.add_argument("-c", "--codec", type=str, default="MJPG",
    help="codec of output video")
args = vars(ap.parse_args())


if __name__=="__main__":
    context = zmq.Context.instance()
    socket = context.socket(zmq.SUB)
    
    print("CONNECT")
    socket.connect("tcp://192.168.1.151:9000")
    socket.subscribe("")
    print("connected")

    fourcc = cv2.VideoWriter_fourcc(*args["codec"])
    out = cv2.VideoWriter('cube.avi', -1, 20.0, (640,480))
    writer = None


    k = 0

    while True:
    #for i in range(20):
        string = socket.recv_multipart(2)
        if (string[0].decode("utf-8")=="Image"):
            msg=ImageInfo()
            msg.ParseFromString(string[1])
             
            image_data=msg.image
            image=Image.frombytes('RGB', (640,480), image_data)
            
            image = numpy.array(image)
            
            #cv stuff here
            frame = image

            if writer is None:
                # store the image dimensions, initialzie the video writer,
                # and construct the zeros array
                (h, w) = frame.shape[:2]
                writer = cv2.VideoWriter(args["output"], fourcc, args["fps"],
                    (w, h), True)

            output = frame
            writer.write(output)


            cv2.imshow("Stream", image)
            key = cv2.waitKey(30) & 0xff
            if key == 27:
               break
            print ("recieved " + str(k))
            k = k + 1
    writer.release()
    cv2.destroyAllWindows()