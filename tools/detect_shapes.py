import zmq
import numpy as np
import sys
import cv2
import time, math
import base64
from random import randint
from Message_pb2 import *
#import protobuf3 as pb
from PIL import Image
import imutils
#from shapedetector import ShapeDetector
from condet import ShapeComparer
k = 0


def threshold(last_cnts):
    ballLower = (16, 0, 176)
    ballUpper = (164, 255, 255)

    greenLower = (25, 53, 80)
    greenUpper = (91, 255, 255)

    greenhomeLower = (34, 41, 107)
    greenhomeUpper = (92, 165, 255)

    greenhomeLower = (25, 41, 80)
    greenhomeUpper = (92, 255, 255)

    green2Lower = (63, 51, 80)
    green2Upper = (90, 255, 255)

    cubeLower = (50, 0, 10)
    cubeUpper = (255, 255, 75)

    cube2Lower = (110, 20, 0)
    cube2Upper = (255, 255, 75)

    cube3Lower = (133, 25, 50)
    cube3Upper = (255, 255, 255)


    starLower = (19, 49, 162)
    starUpper = (31, 255, 255)


    blurred = cv2.GaussianBlur(image, (11, 11), 0)
    hsv = cv2.cvtColor(blurred, cv2.COLOR_BGR2HSV)

    mask = cv2.inRange(hsv, greenhomeLower, greenhomeUpper)
    #mask = cv2.inRange(hsv, ballLower, ballUpper)
    #mask = cv2.inRange(hsv, starLower, starUpper)
    #mask = cv2.inRange(hsv, cube3Lower, cube3Upper)
    mask = cv2.erode(mask, None, iterations=2)
    mask = cv2.dilate(mask, None, iterations=2)

    res = cv2.bitwise_and(image,image,mask = mask)
    cv2.imshow("res", res)

    cimg = image.copy()

    cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    cnts = imutils.grab_contours(cnts)

    #sd = ShapeDetector()
    sc = ShapeComparer()
    #cv2.drawContours(cimg, cnts, -1, (0,255,0), 3)

    if len(cnts) > 0:

        cnts = sorted(cnts, key = cv2.contourArea) [-5:]

        if last_cnts is None:
            last_cnts = cnts
            print ("filles")


        comp_cnts = []
        for c,c_old in zip(cnts,last_cnts):
            #print (cv2.matchShapes(c,c_old,1,0.0))
            if cv2.matchShapes(c,c_old,1,10.0) < 0.05 and cv2.contourArea(c) > 200:# and \
                comp_cnts.append((c, c_old))
        comp_cnts = sorted(comp_cnts,key = lambda x: 
            (abs(int(cv2.moments(x[0])["m10"] / cv2.moments(x[0])["m00"])
            -int(cv2.moments(x[1])["m10"] / cv2.moments(x[1])["m00"]))
            +abs(int(cv2.moments(x[0])["m01"] / cv2.moments(x[0])["m00"])
            -int(cv2.moments(x[1])["m01"] / cv2.moments(x[1])["m00"])))
            * 1/(cv2.matchShapes(x[0],x[1],1,10.0)+0.00001)
            )
        if len(comp_cnts) > 0:
            tracked_cnts, _ = zip(*comp_cnts)

            obj_cnts = tracked_cnts[-1]
            cv2.drawContours(cimg, [obj_cnts], -1, (0,0,255), 3)

            x, y, w, h = cv2.boundingRect(obj_cnts)
            cv2.rectangle( cimg, (x, y), (x + w, y + h), (255, 0, 0), 2, 8, 0 )  #bounding box

            #shape = sd.detect(obj_cnts)
            shape = sc.detect(obj_cnts)
            print(shape)


        if k > 100:
                #cv2.waitKey(0)
                pass

        last_cnts = cnts


    cv2.imshow("count", cimg)
    return last_cnts



if __name__=="__main__":
    #cap = cv2.VideoCapture('star1.avi')       #device 0 is video capture device  
    #cap = cv2.VideoCapture('greenball.avi') 
    cap = cv2.VideoCapture(0)
    last_cnts = None

    while True:
        ret, image = cap.read()
        #cv stuff here
        last_cnts = threshold(last_cnts)
        #cv2.imshow("Stream", frame)
        key = cv2.waitKey(30) & 0xff
        if key == 27 or key == ord('q'):
           break
        #print ("recieved " + str(k))
        k = k + 1
    cap.release()
    cv2.destroyAllWindows()