# import the necessary packages
import cv2
import numpy as np
import imutils
from PIL import Image

class Shape(object):
    """docstring for Shape"""
    def __init__(self, name, img):

        self.name = name
        
        if name != "unidentified":
            self.img = img
            self.cnts = self.get_contours()
            #self.draw()
    
    def get_contours(self):

        blurred = cv2.GaussianBlur(self.img, (11, 11), 0)
        hsv = cv2.cvtColor(blurred, cv2.COLOR_BGR2HSV)

        mask = cv2.inRange(hsv, (0, 0, 1), (255, 255, 255))
        mask = cv2.erode(mask, None, iterations=2)
        mask = cv2.dilate(mask, None, iterations=2)


        cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        cnts = imutils.grab_contours(cnts)
        return cnts

    def draw(self):
        cimg = self.img.copy()
        cv2.drawContours(cimg, self.cnts, -1, (0,0,255), 3)
        cv2.imshow(self.name, cimg)

    def compare(self, contour):
        best_cnt = sorted(self.cnts, key = cv2.contourArea) [0]
        return cv2.matchShapes(best_cnt,contour,1,10.0)


class ShapeComparer:
    def __init__(self):

        ball = Shape("Ball", cv2.imread("ball.png"))
        cube = Shape("Cube", cv2.imread("cube.png"))
        star = Shape("Star", cv2.imread("star.png"))
        self.unidentified = Shape("unidentified", np.zeros(shape=(5,2)))

        self.shapes = []
        self.shapes.extend((ball,cube,star))

    def detect(self, c):
        best_shape = self.unidentified.name 
        best_score = 1

        for shape in self.shapes:
            score = shape.compare(c)

            if score < best_score:
                best_score = score
                best_shape = shape.name
                #print (score, best_shape)

        return best_shape