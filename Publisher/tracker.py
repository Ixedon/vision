from init import *
from condet import ShapeComparer


class Tracker():
    """docstring for Tracker"""
    def __init__(self, track = True):
        self.last_cnts = None
        self.image = None
        self.track = track

    def run(self,image = None):
        if self.track:
            ret = self.tracker(image)
        else:
            ret = self.no_tracker()
        return ret

    def tracker(self,image):

        self.image = image
        msg = ImageInfo()           #create a imageinfo msg

        shape,obj_cnts,conf = self.threshold()

        try:
          msg.bbX, msg.bbY, msg.bbWidth, msg.bbHeight = cv2.boundingRect(obj_cnts)
        except TypeError:
          pass
        msg.image = image.tostring()

        return (msg,shape,conf)

    def no_tracker(self,image):
        msg = ImageInfo()           #create a imageinfo msg
        msg.image = image.tostring()
        return (msg,"No tracking")

    def threshold(self):

        green2Lower = (63, 51, 80)
        green2Upper = (90, 255, 255)

        greenhomeLower = (25, 41, 80)
        greenhomeUpper = (92, 255, 255)

        cubeLower = (50, 0, 10)
        cubeUpper = (255, 255, 75)

        cube2Lower = (110, 20, 0)
        cube2Upper = (255, 255, 75)

        cube3Lower = (133, 25, 50)
        cube3Upper = (255, 255, 255)


        starLower = (19, 49, 162)
        starUpper = (31, 255, 255)


        blurred = cv2.GaussianBlur(self.image, (11, 11), 0)
        hsv = cv2.cvtColor(blurred, cv2.COLOR_BGR2HSV)


        #mask = cv2.inRange(hsv, starLower, starUpper)
        mask = cv2.inRange(hsv, green2Upper, green2Upper)
        mask = cv2.erode(mask, None, iterations=2)
        mask = cv2.dilate(mask, None, iterations=2)

        cimg = self.image.copy()

        cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        cnts = imutils.grab_contours(cnts)

        sc = ShapeComparer()
        shape = "No shape"
        obj_cnts = None
        conf = 1
        if len(cnts) > 0:

            cnts = sorted(cnts, key = cv2.contourArea) [-5:]

            if self.last_cnts is None:
                self.last_cnts = cnts
                #print ("filles") 


            comp_cnts = []
            for c,c_old in zip(cnts,self.last_cnts):
                if cv2.matchShapes(c,c_old,1,10.0) < 0.05 and cv2.contourArea(c) > 200:# and \
                    comp_cnts.append((c, c_old))
            comp_cnts = sorted(comp_cnts,key = lambda x: 
                (abs(int(cv2.moments(x[0])["m10"] / cv2.moments(x[0])["m00"])
                -int(cv2.moments(x[1])["m10"] / cv2.moments(x[1])["m00"]))
                +abs(int(cv2.moments(x[0])["m01"] / cv2.moments(x[0])["m00"])
                -int(cv2.moments(x[1])["m01"] / cv2.moments(x[1])["m00"])))
                * 1/(cv2.matchShapes(x[0],x[1],1,10.0)+0.00001)
                )
            if len(comp_cnts) > 0:
                tracked_cnts, _ = zip(*comp_cnts)

                obj_cnts = tracked_cnts[-1]
                # cv2.drawContours(cimg, [obj_cnts], -1, (0,0,255), 3)

                # x, y, w, h = cv2.boundingRect(obj_cnts)
                # cv2.rectangle( cimg, (x, y), (x + w, y + h), (255, 0, 0), 2, 8, 0 )  #bounding box

                #shape = sd.detect(obj_cnts)
                shape,conf = sc.detect(obj_cnts)
                #print(shape)


            self.last_cnts = cnts


        return (shape,obj_cnts,conf)



    