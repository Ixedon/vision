import zmq 
import time
import cv2
import imutils
import numpy as np
from random import randint
from Message_pb2 import *
from condet import ShapeComparer
from tracker import Tracker



def threshold(image,last_cnts):

    green2Lower = (63, 51, 80)
    green2Upper = (90, 255, 255)

    greenhomeLower = (25, 41, 80)
    greenhomeUpper = (92, 255, 255)

    cubeLower = (50, 0, 10)
    cubeUpper = (255, 255, 75)

    cube2Lower = (110, 20, 0)
    cube2Upper = (255, 255, 75)

    cube3Lower = (133, 25, 50)
    cube3Upper = (255, 255, 255)


    starLower = (19, 49, 162)
    starUpper = (31, 255, 255)


    blurred = cv2.GaussianBlur(image, (11, 11), 0)
    hsv = cv2.cvtColor(blurred, cv2.COLOR_BGR2HSV)


    #mask = cv2.inRange(hsv, starLower, starUpper)
    mask = cv2.inRange(hsv, greenhomeLower, greenhomeUpper)
    mask = cv2.erode(mask, None, iterations=2)
    mask = cv2.dilate(mask, None, iterations=2)

    cimg = image.copy()

    cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    cnts = imutils.grab_contours(cnts)

    sc = ShapeComparer()
    shape = "No shape"
    obj_cnts = None
    if len(cnts) > 0:

        cnts = sorted(cnts, key = cv2.contourArea) [-5:]

        if last_cnts is None:
            last_cnts = cnts
            print ("filles")


        comp_cnts = []
        for c,c_old in zip(cnts,last_cnts):
            if cv2.matchShapes(c,c_old,1,10.0) < 0.05 and cv2.contourArea(c) > 200:# and \
                comp_cnts.append((c, c_old))
        comp_cnts = sorted(comp_cnts,key = lambda x: 
            (abs(int(cv2.moments(x[0])["m10"] / cv2.moments(x[0])["m00"])
            -int(cv2.moments(x[1])["m10"] / cv2.moments(x[1])["m00"]))
            +abs(int(cv2.moments(x[0])["m01"] / cv2.moments(x[0])["m00"])
            -int(cv2.moments(x[1])["m01"] / cv2.moments(x[1])["m00"])))
            * 1/(cv2.matchShapes(x[0],x[1],1,10.0)+0.00001)
            )
        if len(comp_cnts) > 0:
            tracked_cnts, _ = zip(*comp_cnts)

            obj_cnts = tracked_cnts[-1]
            # cv2.drawContours(cimg, [obj_cnts], -1, (0,0,255), 3)

            # x, y, w, h = cv2.boundingRect(obj_cnts)
            # cv2.rectangle( cimg, (x, y), (x + w, y + h), (255, 0, 0), 2, 8, 0 )  #bounding box

            #shape = sd.detect(obj_cnts)
            shape = sc.detect(obj_cnts)
            print(shape)


        last_cnts = cnts


    return (cimg, last_cnts, shape,obj_cnts)



def tracker(image,last_cnts):

    msg = ImageInfo()           #create a imageinfo msg

    image,last_cnts,shape,obj_cnts = threshold(image,last_cnts)

    try:
      msg.bbX, msg.bbY, msg.bbWidth, msg.bbHeight = cv2.boundingRect(obj_cnts)
    except TypeError:
      pass
    msg.image = image.tostring()

    return (msg,last_cnts,shape)

def no_tracker(image):
    msg = ImageInfo()           #create a imageinfo msg
    msg.image = image.tostring()
    return msg

def main():

    cameraID = 1
    # if you call this script from the command line (the shell) it will run the 'main' function

    # Create a ZMQ_PUB socket and bind publisher to all interfaces
    context = zmq.Context()
    publisher = context.socket(zmq.PUB)
    publisher.bind("tcp://*:%i" % 9000)

    k = 0
    cap = cv2.VideoCapture(0)           #device 0 is video capture device

    last_cnts = None

    trk = Tracker()

    while True:
        ret, frame = cap.read()         #capture images from the camera
                   
        
        

        #msg,last_cnts,shape = tracker(frame,last_cnts)

        msg, shape,conf = trk.run(frame)
        msg.cameraID = cameraID


        binaryMsg = msg.SerializeToString()             #serialize the message using protocol buffers
        publisher.send_multipart([str.encode("Image"), binaryMsg])      #send the message using ZMQ and a certain topic
        
        if(msg.bbHeight!=0):                    #if a contour was send, send also a control message
            cmd = AdditionalControl()
            cmd.command = "Tracking at cam %i"%cameraID + " " + shape
            cmd.cameraID = cameraID
            binaryCmd = cmd.SerializeToString()
            publisher.send_multipart([str.encode("Command"), binaryCmd])    #control messages have a different topic
            print ("Detected " + shape)
         
        print ("send", k)
        k = k +1


if __name__ == "__main__":
    main()


