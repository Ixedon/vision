from init import *
class Leader():
	"""docstring for Leader"""
	def __init__(self,ncam):
		self.n_cam = ncam
		self.init_connection()
		#self.communication()

	def init_connection(self):
		context = zmq.Context() 	# Create a ZMQ_PUB socket and bind publisher to all interfaces

		self.publisher = context.socket(zmq.PUB)
		#self.publisher.bind("tcp://127.0.0.1:8000")
		self.publisher.bind("tcp://*:%i" % 7000)


		self.subsciber = context.socket(zmq.SUB)
		self.subsciber.connect("tcp://192.168.0.101:6000")
		self.subsciber.connect("tcp://192.168.0.100:6000")
		#self.subsciber.connect("tcp://192.168.1.151:8000")
		#self.subsciber.connect("tcp://192.168.1.153:8000")
		self.subsciber.subscribe("Command")

	def send_request(self):
		cmd = AdditionalControl()
		cmd.command = "Request"
		cmd.cameraID = 0
		binaryCmd = cmd.SerializeToString()
		self.publisher.send_multipart([str.encode("Command"), binaryCmd])


	def receive_message(self):

		start = time.time()

		while True:
			try:
				string = self.subsciber.recv_multipart(flags = zmq.NOBLOCK)
				break
			except zmq.ZMQError:
				if time.time() - start > 0.3:   #time out if some message not received
					return ("No signal", 10)

		pred = Prediction()
		pred.ParseFromString(string[1])

		print(str(pred.cameraID) + " " + str(pred.shape)+" " + str(pred.confidence))

		return (pred.shape,pred.confidence)


	def vote(self,preds):
		sorted_preds = sorted(preds.items(), key=lambda kv: kv[1])
		best_pred = sorted_preds[-1][0]
		
		print ("Best pred: " + str(best_pred)+ "\n")
		return best_pred

	def communication(self):
		while True:

			predictions = {}

			self.send_request()
			for i in range(0,self.n_cam):
				pred,conf = self.receive_message()

				if pred in predictions:
					predictions[pred]+=1/conf
				else:
					predictions[pred]=1/conf

			best_pred = self.vote(predictions)
			time.sleep(2)



#lead = Leader()


