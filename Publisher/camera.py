from init import *
from tracker import Tracker
class Camera():
	"""docstring for Camera"""
	def __init__(self,cameraID,leadID):
		
		self.cameraID = cameraID

		self.init_connection()
		self.init_camera()
		self.start_tracker()
		self.communication()

	def init_connection(self):
		context = zmq.Context() 	# Create a ZMQ_PUB socket and bind publisher to all interfaces
		# self.image_publisher = context.socket(zmq.PUB)
		# self.image_publisher.bind("tcp://*:%i" % 8000)

		self.publisher = context.socket(zmq.PUB)
		#self.publisher.bind("tcp://143.205.186.155:8000")
		self.publisher.bind("tcp://*:%i" % 6000)

		self.subsciber = context.socket(zmq.SUB)
		#self.subsciber.connect("tcp://127.0.0.1:7000")
		self.subsciber.connect("tcp://192.168.0.101:7000")
		#self.subsciber.connect("tcp://192.168.1.152:8000")
		self.subsciber.subscribe("Command")


	def init_camera(self):
		#self.cameraID = int(getpass.getuser()[-1])
		self.cap = cv2.VideoCapture(0)			#device 0 is video capture device


	def send_to_gui(self, msg):
		
		# msg = ImageInfo()			#create a imageinfo msg
		# msg.image = image.tostring()
		msg.cameraID = self.cameraID
		#msg.bbX, msg.bbY, msg.bbWidth, msg.bbHeight = bound_rec
		binaryMsg = msg.SerializeToString()        		#serialize the message using protocol buffers
		self.publisher.send_multipart([str.encode("Image"), binaryMsg])   	#send the message using ZMQ and a certain topic
		
		# if(msg.bbHeight!=0):					#if a contour was send, send also a control message
		# 		cmd = AdditionalControl()
		# 		cmd.command = "Tracking at cam %i"%cameraID
		# 		cmd.cameraID = cameraID
		# 		binaryCmd = cmd.SerializeToString()
		# 		self.publisher.send_multipart([str.encode("Command"), binaryCmd]) 	#control messages have a different topic
		# 		print ("Detected")
	
	# def tracker (self, cos):

	# 	return None
	# 	#cv2.boundingRect(c)

	def run_tracker(self):
		k = 0
		trk = Tracker()
		while True:
			ret, self.frame = self.cap.read()			#capture images from the camera	   
			#bound_rec = self.tracker(self.frame)
			msg, self.shape, self.conf = trk.run(self.frame)
			self.send_to_gui(msg)
			#self.send_to_gui(self.frame, bound_rec)
			#print ("send", k)
			#k = k +1
			
	def start_tracker(self):
		self.shape = "No shape"
		self.conf = 1.0
		self.stopEvent = threading.Event()
		self.thread = threading.Thread(target=self.run_tracker, args=())
		self.thread.start()

	def communication(self):

		while True:
			self.receive_message()
			self.send_message()

	def receive_message(self):
		string = self.subsciber.recv_multipart()

		cmd = AdditionalControl()
		cmd.ParseFromString(string[1])
		print(cmd.command)

	def send_message(self):
		
		# cmd = AdditionalControl()
		# #cmd.command = "OK " + str(self.cameraID)
		# cmd.command = self.shape + " from " + str(self.cameraID)
		# cmd.cameraID = 1

		print ("conf", self.conf)
		pred  = Prediction()
		pred.cameraID = self.cameraID
		pred.shape = self.shape
		pred.confidence = self.conf

		binaryPred = pred.SerializeToString()
		self.publisher.send_multipart([str.encode("Command"), binaryPred])
		#print ("dsa")
		



	
#cam = Camera(2)
# while True:
# 	cam.receive_message()
# 	cam.send_message()