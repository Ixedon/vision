# Visual Senor Networks Lab project collection
### made by Michal Gilski for VSN Lab in 2019

## Files contain solutions to class assignments, DO NOT SHARE PUBLICALY

## Notes:

To run a script execute it by ./'script_name' [OPTION] (you may have to firstly run chmod u+x *.sh, to make them executable)
The misc folder contains some unstable/partly finished scripts (mainly to run code on cameras automatically)
## Structure:


turn_on.sh - turn all cameras on ( cam1-turn_on.sh on camera 1 in cam1 user folder)
cam1-turn_on.sh - put on camera 1 in cam1 user folder to use command above (names as turn_on.sh)

connect.sh [camera_num] - connects to a specific camera and opens demoVSN/ty
tab_connect.sh [camera_num] - connects to a specific camera in new tab demoVSN/ty (requres gnome, tested on fedora)
make_terminals.sh - opens all five cameras in new tabs and opens demoVSN/ty (requres gnome, tested on fedora)

transfer.sh [folder_name (from folder above), default - transfer] [cam_num] - transfers a folder to the camera demoVSN/ty folder
all_transfer.sh [folder_name (same as above)] - transfers a folder to all cameras demoVSN/ty folder

delete.sh - deletes all cameras folder demoVSN/ty contents
turn_off.sh - turns off all cameras except cam1

## Example usage:

./all_transfer.sh final-project
./connect 1
./transfer final-project 2 3 (handles multiple camera numbers)
./transfer 1 (transfers folder transfer by default)

