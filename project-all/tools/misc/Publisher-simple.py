import zmq 
import time
import cv2
import base64
import imutils
import numpy as np
from random import randint
from Message_pb2 import *





def tracker(image):

	msg = ImageInfo()			#create a imageinfo msg

	greenLower = (46, 121, 0)
	greenUpper = (90, 255, 255)

	#image = imutils.resize(image, width=600)
	blurred = cv2.GaussianBlur(image, (11, 11), 0)
	hsv = cv2.cvtColor(blurred, cv2.COLOR_BGR2HSV)

	mask = cv2.inRange(hsv, greenLower, greenUpper)
	mask = cv2.erode(mask, None, iterations=2)
	mask = cv2.dilate(mask, None, iterations=2)


	cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL,
		cv2.CHAIN_APPROX_SIMPLE)
	cnts = imutils.grab_contours(cnts)
	#cv2.drawContours(image, cnts, -1, (0,255,0), 3)


	center = None
 
	# only proceed if at least one contour was found
	if len(cnts) > 0:
		# find the largest contour in the mask, then use
		# it to compute the minimum enclosing circle and
		# centroid
		c = max(cnts, key=cv2.contourArea)
		((x, y), radius) = cv2.minEnclosingCircle(c)
		
		M = cv2.moments(c)
		center = (int(M["m10"] / M["m00"]), int(M["m01"] / M["m00"]))
 
		msg.bbX, msg.bbY, msg.bbWidth, msg.bbHeight = cv2.boundingRect(c)

		# only proceed if the radius meets a minimum size
		if radius > 10:
			pass
			# draw the circle and centroid on the frame,
			# then update the list of tracked points
			#cv2.circle(image, (int(x), int(y)), int(radius),(0, 255, 255), 2)     #boundig circle
			cv2.circle(image, center, 5, (0, 0, 255), -1)                          # center of bounding circle
			#cv2.rectangle( image, (msg.bbX, msg.bbY), (msg.bbX + msg.bbWidth, msg.bbY + msg.bbHeight), (255, 0, 255), 2, 8, 0 )  #bounding box

 
	# update the points queue
	#pts.appendleft(center)

	msg.image = image.tostring()

	return msg

def no_tracker(image):
	msg = ImageInfo()			#create a imageinfo msg
	msg.image = image.tostring()
	return msg

def main():

	cameraID = 1
	# if you call this script from the command line (the shell) it will run the 'main' function

	# Create a ZMQ_PUB socket and bind publisher to all interfaces
	context = zmq.Context()
	publisher = context.socket(zmq.PUB)
	publisher.bind("tcp://*:%i" % 9000)

	k = 0
	cap = cv2.VideoCapture(0)			#device 0 is video capture device

	while True:
		ret, frame = cap.read()			#capture images from the camera
				   
		
		
		#msg.image = frame.tostring()

		#frame = tracker(frame)
		msg = tracker(frame)
		#msg = no_tracker(frame)

		msg.cameraID = cameraID

		#msg.image = frame.tostring()

		binaryMsg = msg.SerializeToString()        		#serialize the message using protocol buffers
		publisher.send_multipart([str.encode("Image"), binaryMsg])   	#send the message using ZMQ and a certain topic
		
		if(msg.bbHeight!=0):					#if a contour was send, send also a control message
			cmd = AdditionalControl()
			cmd.command = "Tracking at cam %i"%cameraID
			cmd.cameraID = cameraID
			binaryCmd = cmd.SerializeToString()
			publisher.send_multipart([str.encode("Command"), binaryCmd]) 	#control messages have a different topic
			print ("Detected")
		 
		print ("send", k)
		k = k +1


if __name__ == "__main__":
	main()


