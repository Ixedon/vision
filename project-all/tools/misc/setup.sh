#alias python='usr/bin/python3'
#alias pip='usr/bin/pip3'

#pip install virtualenv virtualenvwrapper

echo -e "\n# virtualenv and virtualenvwrapper" >> ~/.bashrc
echo "export WORKON_HOME=$HOME/.virtualenvs" >> ~/.bashrc
echo "source /usr/local/bin/virtualenvwrapper.sh" >> ~/.bashrc

source ~/.bashrc

mkvirtualenv vision -p python3

pip install opencv-contrib-python
pip install pyzmq
pip install protobuf
pip install imutils
pip install Pillow
#apt-get install python3-tk