from init import *
from random import randint

class Election():
	"""docstring for Election"""
	def __init__(self):
		self.n_cam = 2
		self.pid = randint(10, 10000)
		self.init_connection()
		self.create_empty_message()
		#self.communication()
		#self.process()


	def create_empty_message(self):
		emp = ElectionMsg()
		emp.pid = 0
		emp.cameraID = 0
		self.empty = [str.encode("Election"), emp.SerializeToString()]

	def init_connection(self):
		context = zmq.Context() 	# Create a ZMQ_PUB socket and bind publisher to all interfaces

		self.publisher = context.socket(zmq.PUB)
		#self.publisher.bind("tcp://127.0.0.1:8000")
		self.publisher.bind("tcp://*:%i" % 5000)


		self.subsciber = context.socket(zmq.SUB)
		#self.subsciber.connect("tcp://192.168.0.101:5000")
		self.subsciber.connect("tcp://192.168.0.100:5000")
		#self.subsciber.connect("tcp://192.168.1.151:8000")
		#self.subsciber.connect("tcp://192.168.1.153:8000")
		self.subsciber.subscribe("Election")
		self.subsciber.subscribe("Anwser")
		self.subsciber.subscribe("Victory")


	def receive_message(self):

		start = time.time()

		while True:
			try:
				string = self.subsciber.recv_multipart(flags = zmq.NOBLOCK)
				#print("received")
				break
			except zmq.ZMQError:
				if time.time() - start > 0.5:   #time out if some message not received
					return self.empty
		return string



	def send_election(self):
		cmd = ElectionMsg()
		cmd.pid = self.pid
		cmd.cameraID = 1
		binaryCmd = cmd.SerializeToString()
		self.publisher.send_multipart([str.encode("Election"), binaryCmd])


	def send_anwser(self):
		cmd = ElectionMsg()
		cmd.pid = self.pid
		cmd.cameraID = 1
		binaryCmd = cmd.SerializeToString()
		self.publisher.send_multipart([str.encode("Anwser"), binaryCmd])

	def send_victory(self):
		cmd = ElectionMsg()
		cmd.pid = self.pid
		cmd.cameraID = 1
		binaryCmd = cmd.SerializeToString()
		self.publisher.send_multipart([str.encode("Victory"), binaryCmd])




	def communication(self):
		
		time.sleep(3)

		while True:
			self.process()

			time.sleep(5)
			print ("\n")


	def process(self):
		self.pid = randint(10, 10000)

		self.send_election()

		winner = True
		maxpid = 0
		for i in range(0,self.n_cam -1):
			#print("caos")
			string = self.receive_message()            #n elections

			elec = ElectionMsg()
			elec.ParseFromString(string[1])

			if elec.pid > self.pid:
				winner = False

		if winner:
			self.send_victory()
			print ("Victory\n")
			
			return 0
		else:
			string = self.receive_message()
			if string[0].decode("utf-8")=="Victory":
				elec = ElectionMsg()
				elec.ParseFromString(string[1])
				print("Leader is "+ str(elec.cameraID) + "\n")

				return elec.cameraID



def main():

	ele = Election()

if __name__ == "__main__":
	main()
