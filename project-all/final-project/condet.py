# import the necessary packages
import cv2
import numpy as np
import imutils
from glob import glob
from PIL import Image

class Shape():
    """Shape is class for a specific shape that can be recognized"""
    def __init__(self, name, path):

        self.name = name
        self.cnts = []
        self.imgs = []

        if name != "unidentified":
            self.load(path)
    

    def load(self, path):
        # Load model image and find its contours
        path = path + "/*.png"
        files=glob(path)  
        for file in files:
            img = cv2.imread(file)
            self.imgs.append(img)
            self.cnts.append(self.get_contour(img))


    def get_contour(self, img):

   		# Get contours based on the cut out model image
        blurred = cv2.GaussianBlur(img, (11, 11), 0)
        hsv = cv2.cvtColor(blurred, cv2.COLOR_BGR2HSV)

        mask = cv2.inRange(hsv, (0, 0, 1), (255, 255, 255))
        mask = cv2.erode(mask, None, iterations=2)
        mask = cv2.dilate(mask, None, iterations=2)


        cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        cnts = imutils.grab_contours(cnts)
        cnt = sorted(cnts, key = cv2.contourArea) [0]
        
        return cnt

    def draw(self):
    	# Draw the model contours (for debugging)
        cimg = self.img.copy()
        cv2.drawContours(cimg, self.cnts, -1, (0,0,255), 3)
        cv2.imshow(self.name, cimg)

    def compare(self, contour):
    	# Compare an outside contour to the shape's contour
        best_score = 1
        for cnt in self.cnts:
            score = cv2.matchShapes(cnt,contour,1,10.0)
            if score < best_score:
                best_score = score
        # Return the minimal difference value (lower is more simmilar)
        return best_score


class ShapeDetector():
    def __init__(self):

    	# Create the shape models including an 'undefined state'
        ball = Shape("Ball", "models/ball")
        cube = Shape("Cube", "models/cube")
        star = Shape("Star", "models/star")
        self.unidentified = Shape("unidentified", np.zeros(shape=(5,2)))

        # Make an array of recognizable shapes
        self.shapes = []
        self.shapes.extend((ball,cube,star))

    def detect(self, c):
    	# Callable function that predicts the shape of a given contour
        
        # Set up a deafult shape and maximum value that stays if contour doesn't resemble a known shape
        best_shape = self.unidentified.name 
        best_score = 1

        # Compare the contour with all shapes
        for shape in self.shapes:
            score = shape.compare(c)

            # Minimize the differece score for shapes
            if score < best_score:
                best_score = score
                best_shape = shape.name

        return (best_shape,best_score)