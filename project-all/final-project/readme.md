Usage:

For all cameras:
-l  --leader - ID of the leader - default is 1

Only for leader camera (the default leader is cam1):
-c  --color - Color to track, available red, yellow, green - default is green
-n  --ncam   - Maximum number of cameras take opinions from - default is 5

Example usage

Run five cameras with cam1 as leader and tracking color yellow:

cam1: python main.py --color yellow
cam2: python main.py
cam3: python main.py


Run three cameras with cam2 as leader and tracking color red:

cam1: python main.py -l 2
cam2: python main.py -l 2 -n 3 -c red
cam3: python main.py -l 2


