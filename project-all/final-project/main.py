from init import *
from camera import Camera
from leader import Leader


def main():
    
    # Get camera ID (last character of user name like "cam2" -> 2)
    camID = int(getpass.getuser()[-1])
    
    #Get the arguments from the parser
    args = get_arguments(camID)
    # Set ID of the leader
    leadID = args['leader'] 
    # Create camera instance 
    cam = Camera(camID,leadID)

    # If the camera is the leader create and run in seperate thread a leader instance
    if leadID == camID:

        color = args ['color']
        ncam = args ['ncam']

        # Create leader that also takes an instance of its camera (for communication with the gui)
        lead = Leader(ncam = ncam, col = color)
        stopEvent = threading.Event()
        # Run the comminucation method of the leader
        thread = threading.Thread(target=lead.communication, args=())
        thread.start()

    # Run the camera in the main thread
    cam.run()
    



def get_arguments(camID):
    # Create parser
    ap = argparse.ArgumentParser()
    ap.add_argument('-c', '--color', required=False, default = 'unspecified',
                    help='Color to track')
    ap.add_argument('-l', '--leader', required=False, type=int, default = 1,
                    help='ID of the leader')
    ap.add_argument('-n', '--ncam', required=False, type=int, default = 5,
                    help='Maximum number of cameras take opinions from')
    args = vars(ap.parse_args())

    if camID == args ['leader'] and args ['color'] == 'unspecified':
        ap.error("This camera is the leader. Please specify the tracking color.")

    if not args['color'] in ['red', 'yellow', 'green', 'r', 'y', 'g', 'unspecified']:
        ap.error("Please specify a correct color [red, yellow or green].")

    if camID != args ['leader'] and (args['color'] != "unspecified" or args['ncam'] != 5):
        ap.error("Only the leader can specify the number of cameras and tracking color.")
    return args

   


if __name__ == "__main__":
    main()

            
            