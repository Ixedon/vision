from init import *
from tracker import Tracker
class Camera():
    """Camera is an instance created for every camera (including the leader).
       It manages the tracking, shape prediction and is in communication with the leader and the GUI"""
    def __init__(self,cameraID,leadID):
        # Initialize the variables, camera and zmq

        self.cameraID = cameraID
        self.leadID = leadID

        self.color = "green"

        self.init_connection()
        self.init_camera()
        

    def run(self):
        # Trigger function for the camera
        self.start_tracker()
        self.communication()


    def init_connection(self):

        # Create a ZMQ_PUB socket
        context = zmq.Context()     

        # Bind publisher to all interfaces
        self.publisher = context.socket(zmq.PUB)
        self.publisher.bind("tcp://*:%i" % 9000)

        # Subscribe to the leader's shape requests
        self.subsciber = context.socket(zmq.SUB)
        self.subsciber.connect("tcp://192.168.1.15" + str(self.leadID) + ":8000")
        self.subsciber.subscribe("Shape request")


    def init_camera(self):
        # setup the camer (device 0 is default video capture device)
        self.cap = cv2.VideoCapture(0)          


    def send_to_gui(self, msg):
        # Send image, tracking bounding box and current prediction to the Gui

        msg.cameraID = self.cameraID
        # Serialize the message using protocol buffers
        binaryMsg = msg.SerializeToString()
        # Send the message using ZMQ and a certain topic
        self.publisher.send_multipart([str.encode("Image"), binaryMsg])     
        
        # If a bounding box was sent, send also a control message
        if(msg.bbHeight!=0):                    
                cmd = AdditionalControl()
                cmd.command = "Shape" + " at cam " + str(self.cameraID) + " :  " + str(self.shape)
                cmd.cameraID = self.cameraID
                binaryCmd = cmd.SerializeToString()
                # Control messages have a different topic
                self.publisher.send_multipart([str.encode("Command"), binaryCmd])

    def create_message_to_gui(self, image, box):
        # Create the message to send to the Gui
        msg = ImageInfo()
        msg.image = image.tostring()
        msg.bbX, msg.bbY, msg.bbWidth, msg.bbHeight = box
        
        return msg

    def run_tracker(self):
    	# Run the tracker (another thread)

        # Initialize tracker
        trk = Tracker()

        while True:
            # Capture images from the camera  
            ret, self.frame = self.cap.read()

            # Get the message to Gui, shape and confidence            
            image, box, shape, self.conf = trk.run(self.frame, self.color)

            # Safely handle threading by using a lock on a shared variable
            self.thread_lock.acquire()
            self.shape = shape
            self.thread_lock.release()

            msg = self.create_message_to_gui(image, box)
           
           # Send image and bounding box to Gui
            self.send_to_gui(msg)


    def start_tracker(self):
        # Start the tracker in a seperate thread 
        # (so it can send images to the gui while waiting for a shape request)
        
        self.shape = "No shape"
        self.conf = 1.0
        self.stopEvent = threading.Event()
        self.thread_lock = threading.Lock()
        self.thread = threading.Thread(target=self.run_tracker, args=())
        self.thread.start()

    def communication(self):
        # Main communication loop
        while True:
            # Wait for shape request
            self.receive_message()

            # Safely handle threading by using a lock on a shared variable
            self.thread_lock.acquire()
            shape = self.shape
            self.thread_lock.release()

            # If shape detected send it to the leader
            if shape != "No shape":
                self.send_message()

    def receive_message(self):

        # Wait for the shape request
        string = self.subsciber.recv_multipart()

        req = ShapeRequest()
        try:
            req.ParseFromString(string[1])
        except DecodeError:
                pass

        # Update the color requested
        self.color = req.color

    def send_message(self):
        # Send reply to the leader
        reply  = ShapeReply()
        reply.cameraID = self.cameraID

        # Safely handle threading by using a lock on a shared variable
        self.thread_lock.acquire()
        reply.shape = self.shape
        self.thread_lock.release()


        reply.confidence = self.conf

        binaryReply = reply.SerializeToString()
        self.publisher.send_multipart([str.encode("Shape reply"), binaryReply])
    