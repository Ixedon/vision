from init import *
class Subscriber():
    def __init__(self):
        # Initialize the camera and zmq
        self.init_connection()

        self.run()
        
    def init_connection(self):

        # Create a ZMQ_PUB socket
        context = zmq.Context()     

        # Bind publisher to all interfaces
        self.subscriber = context.socket(zmq.SUB)
        self.subscriber.connect("tcp://127.0.0.1:9000")
        # Subscribe to types of messages
        self.subscriber.subscribe("")

    def run(self):

        k = 0
        while True:
            string = self.subscriber.recv_multipart()

            if (string[0].decode("utf-8")=="Image"):
                msg = MyImage()
                try:
                    msg.ParseFromString(string[1])
                except DecodeError:
                    pass
                image_data=msg.image

                # Convert to PIL format
                image = Image.frombytes('RGB', (640,480), image_data)  
                # Convert to to cv2 format
                image = numpy.array(image)

                cv2.imwrite("result/" + str(k) + ".jpg", image)
                
                if k == 9:
                    break
                k+=1
            elif (string[0].decode("utf-8")=="Info"):
                inf = Info()
                try:
                    inf.ParseFromString(string[1])
                except DecodeError:
                    pass
                print (inf.command)

            

sub = Subscriber()