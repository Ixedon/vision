from init import *
class Publisher():
    def __init__(self):
        # Initialize the camera and zmq
        self.nodeID = 0
        time.sleep(1) # warm up camera
        self.cap = cv2.VideoCapture(0)
        self.init_connection()

        self.run_capture()

    def init_connection(self):

        # Create a ZMQ_PUB socket
        context = zmq.Context()     

        # Bind publisher to all interfaces
        self.publisher = context.socket(zmq.PUB)
        self.publisher.bind("tcp://*:%i" % 9000)


    def run_capture(self):

        for x in range(0,10):
            print ("sent")
            # Capture images from the camera  
            ret, image = self.cap.read()
            # cv2.imshow("Image", image)
            # cv2.waitKey(0)
            # Create the image message to the Gui           
            msg = MyImage()
            msg.image = image.tostring()
            self.send_to_gui(msg, "Image")

            if not x%2:
                msg = Info()
                msg.command = "Counter  number  is " + str(x)
                self.send_to_gui(msg, "Info")
            time.sleep(3)


    def send_to_gui(self, msg, topic):
        # Send image and node ID to Gui

        msg.nodeID = self.nodeID
        # Serialize the message using protocol buffers
        binaryMsg = msg.SerializeToString()
        # Send the message using ZMQ and a certain topic
        self.publisher.send_multipart([str.encode(topic), binaryMsg])
    
pub = Publisher()