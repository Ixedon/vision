# Visual Senor Networks Lab project template - assignmet 1
### made by Michal Gilski for VSN Lab in 2019

## Usage rules:
-c  --color - Color to track, available red, yellow, green

## Example usage
python main.py --color yellow
python main.py -c red

