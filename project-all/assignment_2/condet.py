# import the necessary packages
import cv2
import numpy as np
import imutils
from PIL import Image

class Shape(object):
    """Shape is class for a specific shape that can be recognized"""
    def __init__(self, name, img):

        self.name = name
        
        # Load model image and find its contours
        if name != "unidentified":
            self.img = img
            self.cnts = self.get_contours()
    
    def get_contours(self):

   		# Get contours based on the cut out model image
        blurred = cv2.GaussianBlur(self.img, (11, 11), 0)
        hsv = cv2.cvtColor(blurred, cv2.COLOR_BGR2HSV)

        mask = cv2.inRange(hsv, (0, 0, 1), (255, 255, 255))
        mask = cv2.erode(mask, None, iterations=2)
        mask = cv2.dilate(mask, None, iterations=2)


        cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        cnts = imutils.grab_contours(cnts)
        return cnts

    def draw(self):
    	# Draw the model contours (for debugging)
        cimg = self.img.copy()
        cv2.drawContours(cimg, self.cnts, -1, (0,0,255), 3)
        cv2.imshow(self.name, cimg)

    def compare(self, contour):
    	# Compare an outside contour to the shape's contour
        best_cnt = sorted(self.cnts, key = cv2.contourArea) [0]

        # Return the difference value (lower is more simmilar)
        return cv2.matchShapes(best_cnt,contour,1,10.0)


class ShapeDetector:
    def __init__(self):

    	# Create the shape models including an 'undefined state'
        ball = Shape("Ball", cv2.imread("models/ball.png"))
        cube = Shape("Cube", cv2.imread("models/cube.png"))
        star = Shape("Star", cv2.imread("models/star.png"))
        self.unidentified = Shape("unidentified", np.zeros(shape=(5,2)))

        # Make an array of recognizable shapes
        self.shapes = []
        self.shapes.extend((ball,cube,star))

    def detect(self, c):
    	# Callable function that predicts the shape of a given contour
        
        # Set up a deafult shape and maximum value that stays if contour doesn't resemble a known shape
        best_shape = self.unidentified.name 
        best_score = 1

        # Compare the contour with all shapes
        for shape in self.shapes:
            score = shape.compare(c)

            # Minimize the differece score for shapes
            if score < best_score:
                best_score = score
                best_shape = shape.name

        return (best_shape,best_score)