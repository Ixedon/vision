from init import *
from tracker import Tracker
class Publisher():
    """Camera is an instance created for every camera (including the leader).
       It manages the tracking, shape prediction and is in communication with the leader and the GUI"""
    def __init__(self, camID, color):
        # Initialize the camera and zmq
        self.shape = "No shape"
        self.conf = 1.0
        self.cameraID = camID
        self.color = color

        self.cap = cv2.VideoCapture(0) 
        self.init_connection()
        
        
    def run(self):
        # Trigger function for the camera
        self.run_tracker()
        self.communication()


    def init_connection(self):

        # Create a ZMQ_PUB socket
        context = zmq.Context()     

        # Bind publisher to all interfaces
        self.publisher = context.socket(zmq.PUB)
        self.publisher.bind("tcp://*:%i" % 9000)


    def run_tracker(self):

        # Initialize tracker
        trk = Tracker()

        while True:
            # Capture images from the camera  
            ret, self.frame = self.cap.read()
            # Get the message to Gui, shape and confidence            
            msg, self.shape, self.conf = trk.run(self.frame, self.color)
            self.send_to_gui(msg)


    def send_to_gui(self, msg):
        # Send image, tracking bounding box and current prediction to the Gui

        msg.cameraID = self.cameraID
        # Serialize the message using protocol buffers
        binaryMsg = msg.SerializeToString()
        # Send the message using ZMQ and a certain topic
        self.publisher.send_multipart([str.encode("Image"), binaryMsg])     
        
        # If a bounding box was sent, send also a control message
        if(msg.bbHeight!=0):                    
                cmd = AdditionalControl()
                cmd.command = "Shape" + " at cam " + str(self.cameraID) + " :  " + str(self.shape)
                cmd.cameraID = self.cameraID
                binaryCmd = cmd.SerializeToString()
                # Control messages have a different topic
                self.publisher.send_multipart([str.encode("Command"), binaryCmd])
    