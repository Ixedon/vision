from init import *
from publisher import Publisher

def main():
	
	# Get camera ID (last character of user name like "cam2" -> 2)
	camID = int(getpass.getuser()[-1])

	#Get the arguments from the parser
	args = get_arguments(camID)
	# Create camera instance 
	pub = Publisher(camID, args['color'])

	# Run the publisher in the main thread
	pub.run()
	



def get_arguments(camID):
	# Create parser
    ap = argparse.ArgumentParser()
    ap.add_argument('-c', '--color', required=True,
                    help='Color to track')
    args = vars(ap.parse_args())

    return args

   

if __name__ == "__main__":
	main()

			
			