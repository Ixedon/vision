# Visual Senor Networks Lab project collection
### made by Michal Gilski for VSN Lab in 2019

## Files contain solutions to class assignments, DO NOT SHARE PUBLICALY

## Notes:
To run the codes on the cameras turn on virtualenv by typing "workon vision" (if it is still installed, if not follow the install_guide.txt in the gui folder) and then run "python main.py"

## Structure:

Assignment 1  - solution to assignment 1

Assignment 2  - solution to assignment 2

Final project - contains the final version of the completed final assignment

Gui - contains the GUI for the class

Tools - contains useful tools/files mainly the range detector (subfolder misc is composed of left over files that are not guaranteed to work / be understandable.

Scripts - useful scripts for managing cameras in the lab (again no guarantees, tested only on Fedora)

tools/3d-models - contains 3d models (in blender) and renders of three template shapes

